#!/bin/bash

# Generates a random string with $1 characters
random_string () {
    # Initialize LEN to the first parameter
    local LEN=$1
    # If LEN is empty set to 16 (default value)
    : ${LEN:=16}
    # Generate random string with $LEN characters
    local STR=$(tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-./:;<=>?@[\]_{|}' </dev/urandom | head -c $LEN  ; echo)
    echo "String: \"$STR\""
}

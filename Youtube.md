Remove all videos from Youtube's Watch Later playlist:  

1. Go to your Watch Later playlist on Youtube on your Browser (tested on Google Chrome).
2. Scroll all the way down until there are no more videos.
3. Open the Devtools Console inside the Watch Later page (Ctrl + shift + i on Windows and Linux).
4. Paste the following script below and press enter.

This will remove each video one by one. **It's likely that Youtube will temporarily block you from deleting videos from your Watch Later playlist after a while. Just refresh the page to stop the script, wait a few minutes (maybe 10 min) and redo the steps above.**

```js
    const videos = Array.from(document.querySelectorAll('ytd-playlist-video-renderer'));
    const interval = setInterval(() => {
    if (!videos.length) {
        clearInterval(interval);
        alert('Done');
        return;
    }
    const video = videos.shift();
    const menu = video.querySelector('#menu #button');
    menu.click();
    setTimeout(() => {
        const remove = document.querySelectorAll('ytd-menu-service-item-renderer[role="menuitem"]')[2];
        if (remove) {
            remove.click();
        }
    }, 500);
}, 1000);

```